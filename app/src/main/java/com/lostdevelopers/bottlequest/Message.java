package com.lostdevelopers.bottlequest;

/**
 * Created by antony on 17/05/17.
 */

public class Message {
    private int id;
    private String message;
    private double lattitude;
    private double longitude;

    public Message(int id, String message, double lattitude, double longitude) {
        this.id = id;
        this.message = message;
        this.lattitude = lattitude;
        this.longitude = longitude;
    }
    public Message(){

    }


    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public double getLattitude() {
        return lattitude;
    }

    public void setLattitude(double lattitude) {
        this.lattitude = lattitude;
    }

    public double getLongitude() {
        return longitude;
    }

    public void setLongitude(double longitude) {
        this.longitude = longitude;
    }
}
