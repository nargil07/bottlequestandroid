package com.lostdevelopers.bottlequest;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import java.util.Random;

public class depositActivity extends AppCompatActivity {

    private EditText message;
    private Button buttonOk;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_deposit);

        message = (EditText)findViewById(R.id.messageText);

        buttonOk.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Random rd = new Random();
                double latitude, longitude;
                latitude = 45+rd.nextDouble();
                longitude = 3+rd.nextDouble();

                depositBottle(message.getText().toString(), latitude, longitude);

            }
        });

    }


    public void depositBottle(String message, double latitude, double longitude) {
        RequestQueue requestQueue = Volley.newRequestQueue(this);
        StringRequest stringRequest = new StringRequest(Request.Method.POST,  ConfigURL.URL+"1/messages?message="+message+"&latitude="+latitude+"&longitude="+longitude, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                if(response.equalsIgnoreCase("true")){

                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e("bottle deposit", error.getMessage());
            }
        });
        requestQueue.add(stringRequest);
    }

}
