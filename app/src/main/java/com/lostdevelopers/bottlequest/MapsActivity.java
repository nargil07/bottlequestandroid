package com.lostdevelopers.bottlequest;

import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentTransaction;

import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;

public class MapsActivity extends FragmentActivity implements OnMapReadyCallback{
    private GoogleMap mMap;

//    private GoogleMap mMap;
//    private Location mLocation = null;
//    private LocationManager locationManager;
//    private int intZoom = 18;
//
//    // record the compass picture angle turned
//    private float currentDegree = 0f;
//
//    // device sensor manager
//    private SensorManager mSensorManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_maps);
//        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
//        // initialize your android device sensor capabilities
//        mSensorManager = (SensorManager) getSystemService(SENSOR_SERVICE);
//        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
//                .findFragmentById(R.id.map);
//        mapFragment.getMapAsync(this);
//        mapFragment.getView().setClickable(false);
//        mapFragment.getView().setFocusable(false);
//
//        // Acquire a reference to the system Location Manager
//        locationManager = (LocationManager) this.getSystemService(Context.LOCATION_SERVICE);
//
        if (ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            ActivityCompat.requestPermissions(this, new String[] {
                            android.Manifest.permission.ACCESS_FINE_LOCATION,
                            android.Manifest.permission.ACCESS_COARSE_LOCATION },
                    1340);
            return;
        }else{
        FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
        fragmentTransaction.add(R.id.layout_maps_container, MapsFragment.newInstance());
        fragmentTransaction.commit();
        }
    }

//    /**
//     * Manipulates the map once available.
//     * This callback is triggered when the map is ready to be used.
//     * This is where we can add markers or lines, add listeners or move the camera. In this case,
//     * we just add a marker near Sydney, Australia.
//     * If Google Play services is not installed on the device, the user will be prompted to install
//     * it inside the SupportMapFragment. This method will only be triggered once the user has
//     * installed Google Play services and returned to the app.
//     */
//    @Override
//    public void onMapReady(GoogleMap googleMap) {
//        mMap = googleMap;
//        addMarquer();
//    }
//
    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if(requestCode == 1340){
            startActivity(new Intent(this, MapsActivity.class));

        }
    }
//    private void addMarquer(){
//        if(mMap !=null){
//            mMap.clear();
//            mMap.addMarker(new MarkerOptions()
//                    .position(new LatLng(mLocation.getLatitude(), mLocation.getLongitude()))
//            .icon(BitmapDescriptorFactory.fromResource(R.drawable.person)));
//        }
//    }

    @Override
    protected void onResume() {
        super.onResume();
//        if (ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
//            // TODO: Consider calling
//            //    ActivityCompat#requestPermissions
//            // here to request the missing permissions, and then overriding
//            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
//            //                                          int[] grantResults)
//            // to handle the case where the user grants the permission. See the documentation
//            ActivityCompat.requestPermissions(this, new String[] {
//                            android.Manifest.permission.ACCESS_FINE_LOCATION,
//                            android.Manifest.permission.ACCESS_COARSE_LOCATION },
//                    1340);
//            return;
//        }else{
//            locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 1, 1, this);
//            mLocation = locationManager.getLastKnownLocation(LocationManager.GPS_PROVIDER);
//            addMarquer();
//        }
//
//        mSensorManager.registerListener(this, mSensorManager.getDefaultSensor(Sensor.TYPE_ORIENTATION),
//                SensorManager.SENSOR_DELAY_GAME);
    }

//    public void onLocationChanged(Location location) {
//        // Called when a new location is found by the network location provider.
//        //makeUseOfNewLocation(location);
//        mLocation = location;
//    }
//
//    public void onStatusChanged(String provider, int status, Bundle extras) {
//    }
//
//    public void onProviderEnabled(String provider) {
//    }
//
//    public void onProviderDisabled(String provider) {
//    }
//
//    @Override
//    public void onFlushCompleted(Sensor sensor) {
//
//    }
//
//    @Override
//    public void onSensorChanged(SensorEvent event) {
//// get the angle around the z-axis rotated
//        mLocation = locationManager.getLastKnownLocation(locationManager
//                .getBestProvider(new Criteria(), false));
//        if(mLocation != null){
//            float degree = Math.round(event.values[0]);
//            CameraPosition currentPlace = new CameraPosition.Builder()
//                    .target(new LatLng(mLocation.getLatitude(), mLocation.getLongitude()))
//                    .bearing(degree).tilt(45.5f).zoom(intZoom).build();
//            mMap.moveCamera(CameraUpdateFactory.newCameraPosition(currentPlace));
//        }
//
//    }
//
//    @Override
//    public void onAccuracyChanged(Sensor sensor, int accuracy) {
//
//    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
    }

}
